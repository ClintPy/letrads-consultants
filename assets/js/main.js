// Sidenav
const sideNav = document.querySelector(".sidenav");
M.Sidenav.init(sideNav, {});


// Slider
const slider = document.querySelector(".slider");
M.Slider.init(slider, {
  indicators: false,
  transition: 500,
  interval: 6000
});



//Modal
$(document).ready(function(){
  $('.modal').modal();
});

$.fn.characterize = function (wrapper, options) {
  var txt = this.text(),
      self = this;

  this.empty();
  
  wrapper = wrapper || '<span />';
  options = options || {};

  Array.prototype.forEach.call(txt, function (c) {
    options.text = c;
    self.append($(wrapper, options));
  });
};

// Title fade
function animate () {
  var wlc = $('#title');

  wlc.characterize('<span />', {
    class: 'fd',
    css: {
      opacity: 0
    }
  });
  
  wlc.css('opacity', 1);

  $('.fd').each(function (i) {
    $(this).animate({opacity: 1}, (i + 1) * 300);
  });
}

animate();

$(document).ready(function () {
  $("#title-big").fadeIn(6000)
})
